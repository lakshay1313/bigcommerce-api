<!DOCTYPE html>
<html>
<head>
    <title>Big Commerce API</title>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href="//netdna.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css" rel="stylesheet">
</head>
<body>
    <div class="container">
  
   
        <form action="{{ url('getCartDetails') }}">
  
            {{ csrf_field() }}
  
            <div class="form-group">
                <label>Cart Id:</label>
                <input type="text" name="cart_id" required class="form-control" placeholder="Cart Id">
            </div>
   
            <div class="form-group">
                <button class="btn btn-success btn-submit">Submit</button>
            </div>
        </form>
    </div>
</body>
</html>