<?php

namespace App\Repositories;

class ApiRequestRepository
{
    public function __constuct()
    {
        //
    }

    //function for public request api
    public function curlRequestResponse($apiUrl, $httpRequestType, $requestType, $postField = false){
        
        $curl = curl_init();
        curl_setopt_array($curl, array(
        CURLOPT_URL => $apiUrl,
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_ENCODING => "",
        CURLOPT_MAXREDIRS => 10,
        CURLOPT_TIMEOUT => 30,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_POSTFIELDS => $postField,
        CURLOPT_CUSTOMREQUEST => $httpRequestType,

        CURLOPT_HTTPHEADER => array(
            "accept: application/json",
            "content-type: application/json",
            "x-auth-token: d59y475fl1cvwylsgiib7kbmdp1hswi"
        ),
        ));

        $response = curl_exec($curl);
        $err = curl_error($curl);

        curl_close($curl);

        if ($err) {
            echo "cURL Error #:" . $err;
        } else {
            return $response; 
        }
    }
}
