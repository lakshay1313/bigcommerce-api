<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Repositories\ApiRequestRepository;

class BigCommerceApiController extends Controller
{
    /*function to get and update the cart Details*/
    public function index(){
        $curl = curl_init();

        curl_setopt_array($curl, array(
          CURLOPT_URL => "https://api.bigcommerce.com/stores/qzkzyqf69h/v3/catalog/products",
          CURLOPT_RETURNTRANSFER => true,
          CURLOPT_ENCODING => "",
          CURLOPT_MAXREDIRS => 10,
          CURLOPT_TIMEOUT => 30,
          CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
          CURLOPT_CUSTOMREQUEST => "GET",
          CURLOPT_HTTPHEADER => array(
            "x-auth-token: d59y475fl1cvwylsgiib7kbmdp1hswi"
          ),
        ));
        
        $response = curl_exec($curl);
        $err = curl_error($curl);
        
        curl_close($curl);
        
        if ($err) {
          echo "cURL Error #:" . $err;
        } else {
          echo $response;
        }
    }


    //cart_id - f47be952-b5db-4d29-9c7b-c09ee503a1b2
    //function to get the card details 
    public function getCartDetails(Request $request){
        
        $apiRequest = new ApiRequestRepository();
        $apiUrl = env('CART_DETAILS_API_URL').$request->post('cart_id');
        $response = $apiRequest->curlRequestResponse($apiUrl, "GET", 1);
        
        if($response){
          $decodeJson = json_decode($response); 
          $physicalItems = isset($decodeJson->data->line_items->physical_items)?$decodeJson->data->line_items->physical_items:'';
          if(!empty($physicalItems)){
            
             $arrayCount = floor(count($physicalItems)/2);
              $col  = 'list_price';
              $sort = array();
              foreach ($physicalItems as $i => $obj) {
                $sort[$i] = $obj->{$col};
              }

              $sorted_db = array_multisort($sort, SORT_ASC, $physicalItems);
  
             foreach(array_slice($physicalItems, 0, $arrayCount) as $key => $value){ 
            
                $updateProductApiUrl = env('CART_DETAILS_API_URL').$request->post('cart_id')."/items/".$value->id;
                $postField = "{\"line_item\":{\"quantity\":".$value->quantity.",\"product_id\":".$value->product_id.",\"list_price\":0}}";
                $result = $apiRequest->curlRequestResponse($updateProductApiUrl, "PUT", 2, $postField );
              
          }
        }
      }
        $response = isset($result)?$result:$response;
        echo "<pre>"; print_r($response); 
    }

    
}
