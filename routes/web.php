<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('form');
});
//route to get the products details
Route::get('getDetails', 'App\Http\Controllers\BigCommerceApiController@index');

//route to get the kart details
Route::get('getCartDetails', 'App\Http\Controllers\BigCommerceApiController@getCartDetails');



